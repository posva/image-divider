#include <SFML/Graphics.hpp>
#include <iostream>
#include <sstream>

bool replace(std::string& str, const std::string& from, const std::string& to) ;

int main (int argc, const char * argv[])
{
	sf::Context context;
	context.setActive(1);
	
	unsigned int divsions;
	std::string file;
	sf::Texture tx;
	std::string divType;
	
	if (argc == 1)
	{
		std::cout<<"Enter the file name: ";
		std::cin>>file;
		std::cout<<"Enter the number of divisions: ";
		std::cin>>divsions;
	}
	else if (argc == 2)
	{
		file = argv[1];
		std::cout<<"Enter the number of divisions: ";
		std::cin>>divsions;
	}
	else if (argc == 3)
	{
		file = argv[1];
		sscanf(argv[2], "%u", &divsions);
	}
	
	if (divsions == 0)
	{
		std::cout<<"The number of divisions must be greater that 0!\n";
		return 1;
	}
	
	std::cout<<"Vertical(n) or horizontal(y) division?: ";
	std::cin>>divType;
	
	if (!tx.loadFromFile(file))
	{
		std::cout<<"The file "<<file<<" cannot be loaded\n";
		return 1;
	}
	
	sf::RenderTexture ren;
	sf::Sprite spr(tx);
	sf::Vector2u size(tx.getSize().x, tx.getSize().y);
	if (divType[0] == 'n')
		size.x /= divsions;
	else
		size.y /= divsions;
	replace(file, ".png", "");
	
	ren.create(size.x, size.y);
	
	for (unsigned int i=0; i<divsions; ++i)
	{
		ren.clear(sf::Color(0,0,0,0));
		
		if (divType[0] == 'n')
			spr.setTextureRect(sf::IntRect(i*size.x, 0, size.x, size.y));
		else
			spr.setTextureRect(sf::IntRect(0, i*size.y, size.x, size.y));
		ren.draw(spr);
		
		ren.display();
		
		std::ostringstream stream;
		stream<<file<<"_"<<i<<".png";
		
		ren.getTexture().copyToImage().saveToFile(stream.str());
		std::cout<<"Created "<<stream.str()<<"\n";
	}
	
	return EXIT_SUCCESS;
}


bool replace(std::string& str, const std::string& from, const std::string& to) {
    size_t start_pos = str.find(from);
    if(start_pos == std::string::npos)
        return false;
    str.replace(start_pos, from.length(), to);
    return true;
}