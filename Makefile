CXX = g++
OBJ = obj/
SRC = src/
BIN = bin/
POINTCPP = $(wildcard $(SRC)*.cpp) $(wildcard $(SRC)*/*.cpp)
POINTOP := $(POINTCPP:.cpp=.o)
POINTO = $(patsubst src/%,$(OBJ)%,$(POINTOP)) #$(POINTOP:src=obj)

OPT := -Wall -pedantic -Wno-long-long -O2 -g -I "$(SRC)"

ifeq ($(SHELL), sh.exe) 
OS := Win
else
OS := $(shell uname)
endif

ifeq ($(OS), Win)
EXEC := imgd
else
EXEC := imgd
endif

ifeq ($(OS), Linux)
RM = rm
LIBS := -lsfml-audio -lsfml-graphics -lsfml-window -lsfml-network -lsfml-system -lboost_system -lboost_filesystem -llua -lGL
endif
ifeq ($(OS), Darwin)
RM = rm
LIBS := -framework sfml-system -framework sfml-window -framework sfml-graphics -framework sfml-audio -framework sfml-network -framework OpenGL -lboost_system -lboost_filesystem -llua5.1
endif
ifeq ($(OS), Win)
RM = del
LIBS := -lsfml-audio -lsfml-graphics -lsfml-window -lsfml-network -lsfml-system -lboost_system-mgw46-mt-1_49 -lboost_filesystem-mgw46-mt-1_49 -lopengl32 -llua
endif


all : run

dirs : 
ifeq ($(OS), Darwin)
	@./configure.sh bin obj src
endif
ifeq ($(OS), Linux)
	@./configure.sh bin obj src
endif
	
ifeq ($(OS), Win)
	@echo "You need to manually create subdirectories"
endif
.PHONY : dirs

cleanall: 
	@$(RM) -vf $(OBJ)*.o $(OBJ)*/*.o $(BIN)*
.PHONY : cleanall

clean: 
	@$(RM) -vf $(OBJ)*.o $(OBJ)Core/*.o $(OBJ)Resources/*.o $(BIN)*
.PHONY : clean

run: dirs $(EXEC)
	@echo "Launching $(EXEC)"
ifeq ($(OS), Darwin)
	@./$(BIN)$(EXEC)
endif
ifeq ($(OS), Linux)
	@./$(BIN)$(EXEC)
endif
ifeq ($(OS), Win)
	@$(BIN)$(EXEC).exe
endif
.PHONY : run


$(OBJ)%.o : $(SRC)%.cpp
	@echo "Compiling $^"
	@$(CXX) $(OPT) $^ -c -o $@


$(EXEC) : $(POINTO)
	@echo "Linking $@"
	@$(CXX) $(OPT) $^ -o $(BIN)$(EXEC) $(LIBS)

valgrind : dirs Test
	valgrind -v --leak-check=full --tool=memcheck ./$(BIN)$(EXEC)